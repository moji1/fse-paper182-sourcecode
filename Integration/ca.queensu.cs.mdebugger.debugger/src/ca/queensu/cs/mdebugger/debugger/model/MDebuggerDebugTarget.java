/*******************************************************************************
 * Copyright (c) 2016 School of Computing -- Queen's University
 *
 * Description: no description currently
 * 
 * Contributors:
 *     Nicolas Hili <hili@cs.queensu.ca> - initial API and implementation
 ******************************************************************************/
package ca.queensu.cs.mdebugger.debugger.model;

import java.io.File;

import org.eclipse.core.resources.IMarkerDelta;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugEvent;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.model.IBreakpoint;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IMemoryBlock;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.debug.core.model.IThread;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.papyrus.editor.PapyrusMultiDiagramEditor;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorRegistry;
import org.eclipse.ui.IPersistableElement;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.emf.common.ui.URIEditorInput;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.queensu.cs.mdebugger.debugger.server.MDebuggerTCPServer;
import ca.queensu.cs.mdebugger.debugger.server.MDebuggerTCPSocket;
import ca.queensu.cs.mdebugger.debugger.server.MDebuggerTCPWriter;

/**
 * @author nicolas
 *
 */
public class MDebuggerDebugTarget extends MDebuggerDebugElement implements IDebugTarget {

	private IProcess process;
    private ILaunch launch;

    private MDebuggerTCPServer tcpServerRunnable;
    private Thread tcpServer;
    
    private IThread[] threads;
    
    private MDebuggerTCPWriter writer;
    private MDebuggerTCPSocket reader;
    
    // The different states, which this IThread can have
    private enum State {
            RUNNING, STEPPING, SUSPENDED, TERMINATED
    }
    
    State currentState;
    
    private PapyrusMultiDiagramEditor editor;

    // the IProcess and ILaunch are provided by an IDebugTarget and therefore
    // they should be passed. The getProcess() and getLaunch() method should be
    // overridden accordingly. See DebugElement implementation
    public MDebuggerDebugTarget(IProcess process, ILaunch launch) {
            super(null);
            this.launch = launch;
            this.tcpServerRunnable = new  MDebuggerTCPServer(8001, this);
            this.tcpServer = new Thread(tcpServerRunnable);
            this.process = process;
            
            this.threads = new IThread[0];

            // notify that this element has been created
            fireCreationEvent();
            
            // define the current state as running
            currentState = State.RUNNING;
            
            this.tcpServer.start();
            
    		String diPath = "";
    		
    		try {
    			diPath = launch.getLaunchConfiguration().getAttribute("ca.queensu.cs.mdebugger.launcher.attributes.diPath", "/PingPong/model.di");
    		} catch (CoreException e) {
    			e.printStackTrace();
    		}
    		
    		 Display.getDefault().syncExec(new PapyrusEditorUtil(this, diPath));
    		 //System.out.println(this.editor);
    		 
/*    		 IEditorRegistry registry = PlatformUI.getWorkbench().getEditorRegistry();
    		 IEditorDescriptor[] desc = registry.getEditors("test.di");
    		 if (desc.length > 0) {
    			 IEditorDescriptor editor = desc[0];
    			 if ("org.eclipse.papyrus.infra.core.papyrusEditor".equals(editor.getId())) {    				 
    				 
    			 }
    		 } */
    }

    // must be overridden since "this" cannot be passed in a constructor
    @Override
    public IDebugTarget getDebugTarget() {
            return this;
    }

    @Override
    public IProcess getProcess() {
            return this.process;
    }

    @Override
    public ILaunch getLaunch() {
            return launch;
    }

    @Override
    public boolean canTerminate() {
            return true;
    }

    @Override
    public boolean isTerminated() {
    	return State.TERMINATED.equals(currentState);
    }

    @Override
    public void terminate() throws DebugException {
    	for (IThread thread: threads) {
    		thread.terminate();
    	}
    	
    	threads = new IThread[0];
    	tcpServerRunnable.close();
    	tcpServer.interrupt();
    	
    	fireTerminateEvent();
    	
    	currentState = State.TERMINATED;
    	
//    	Launch launch = (Launch)getLaunch();
    	
    }
    
    public void setWriter(MDebuggerTCPWriter writer) {
		this.writer = writer;
	}
    
    public void setReader(MDebuggerTCPSocket reader) {
		this.reader = reader;
	}

    @Override
    public boolean canResume() {
    	return isSuspended();
    }

    @Override
    public boolean canSuspend() {
    	return State.RUNNING.equals(currentState);
    }

    @Override
    public boolean isSuspended() {
    	return State.SUSPENDED.equals(currentState);
    }

    @Override
    public void resume() throws DebugException {
            currentState = State.RUNNING;
            for (IThread thread: this.getThreads()) {
            	thread.resume();
            }
            
        	
        	if (this.writer != null)
        		this.writer.requestCapsuleList();
        	
            fireResumeEvent(DebugEvent.CLIENT_REQUEST);
    }

    @Override
    public void suspend() throws DebugException {
    	currentState = State.SUSPENDED;
    	fireSuspendEvent(DebugEvent.BREAKPOINT);
    }

    @Override
    public void breakpointAdded(IBreakpoint breakpoint) {
    }

    @Override
    public void breakpointRemoved(IBreakpoint breakpoint, IMarkerDelta delta) {
    }

    @Override
    public void breakpointChanged(IBreakpoint breakpoint, IMarkerDelta delta) {
    }

    @Override
    public boolean canDisconnect() {
            return false;
    }

    @Override
    public void disconnect() throws DebugException {
    }

    @Override
    public boolean isDisconnected() {
            return false;
    }

    @Override
    public boolean supportsStorageRetrieval() {
            return false;
    }

    @Override
    public IMemoryBlock getMemoryBlock(long startAddress, long length) throws DebugException {
            return null;
    }

    @Override
    public IThread[] getThreads() throws DebugException {
            if (isTerminated()) {
                    return new IThread[0];
            }
            return this.threads;
    }
    
    public IThread getThread(String name) {
    	try {
	    	IThread[] threads = getThreads();
	    	for (int i = 0; i < threads.length; i++) {
	    		CapsuleThread thread = (CapsuleThread)threads[i];
					if (thread != null && thread.getCapsuleName().equals(name))
						return thread;
	    	}
    	} catch (DebugException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return null;
    }

    @Override
    public boolean hasThreads() throws DebugException {
            return (this.threads != null && this.threads.length > 0);
    }

    @Override
    public String getName() throws DebugException {
            return "Example Debug target";
    }

    @Override
    public boolean supportsBreakpoint(IBreakpoint breakpoint) {
            return true;
    }

    public void dispose() {
            fireTerminateEvent();
    }

	public void setCapsule(String[] capsules, String[] currentStates) {
		int n = capsules.length;
		IThread[] threads = new IThread[n];
		
		for (int i = 0; i < n; i++) {
			
			CapsuleThread capsuleThread = new CapsuleThread(this, capsules[i]);
			capsuleThread.setCurrentState(currentStates[i]);
			threads[i] = capsuleThread;
			
		//	this.reader.currentState = MDebuggerTCPSocket.State.WAITING_FOR_EVENT_LIST;
			this.writer.requestEventList(capsules[i], 5);
		}
		this.threads = threads;
	}
	
	public void modifyValue(String capsuleName, String variableName, String value) {
	//	this.reader.currentState = MDebuggerTCPSocket.State.WAITING_FOR_ACK;
		this.writer.modifyValue(capsuleName, variableName, value);
	}

	public void stepOver(String capsuleName) {
		this.writer.stepOver(capsuleName);
	}
	
	public class PapyrusEditorUtil implements Runnable {

		private MDebuggerDebugTarget target;
		private String path;

		public PapyrusEditorUtil(MDebuggerDebugTarget target, String path) {
			this.target = target;
			this.path = path;
		}
		
		@Override
		public void run() {
			try {
				URI uri = URI.createPlatformResourceURI(path, true);
				IEditorInput editorInput = new URIEditorInput(uri);
				IEditorPart papyrusEditor = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().openEditor(editorInput, "org.eclipse.papyrus.infra.core.papyrusEditor");
				this.target.editor = (PapyrusMultiDiagramEditor)papyrusEditor;
			} catch (PartInitException e) {
				e.printStackTrace();
			}
		}
		
	}

}
