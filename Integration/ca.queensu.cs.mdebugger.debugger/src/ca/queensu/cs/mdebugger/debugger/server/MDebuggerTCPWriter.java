/*******************************************************************************
 * Copyright (c) 2016 School of Computing -- Queen's University
 *
 * Description: no description currently
 * 
 * Contributors:
 *     Nicolas Hili <hili@cs.queensu.ca> - initial API and implementation
 ******************************************************************************/
package ca.queensu.cs.mdebugger.debugger.server;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import org.eclipse.debug.core.model.IDebugTarget;

import ca.queensu.cs.mdebugger.debugger.server.MDebuggerTCPSocket.Command;

/**
 * @author nicolas
 *
 */
public class MDebuggerTCPWriter {

	protected Socket socket;
	private IDebugTarget target;
	private DataOutputStream out;
	private MDebuggerTCPSocket reader;
	private static int commandId = 0;
	
	public MDebuggerTCPWriter(Socket clientSocket, IDebugTarget target, MDebuggerTCPSocket reader) {
		this.socket = clientSocket;
		this.target = target;
		this.reader = reader;
		
		System.out.println("Creating new client");
    	
        this.out = null;
        try {
            out = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            return;
        }
	}
	

	public void requestCapsuleList() {
		sendMessage("list Counter:Counter:0 -b",
				MDebuggerTCPSocket.Command.WAITING_FOR_BREAKPOINT_LIST);
	}
	
	public void requestEventList(String capsuleName, int n) {
		sendMessage("v -c "+ capsuleName +" -n " + String.valueOf(n) + " -e",
					MDebuggerTCPSocket.Command.WAITING_FOR_EVENT_LIST);
	}
	
	public void modifyValue(String capsuleName, String variableName, String value) {
		sendMessage("m -c " + capsuleName + " -n " + variableName + " -v " + value,
				MDebuggerTCPSocket.Command.WAITING_FOR_ACK);
	}
	
	public void stepOver(String capsuleName) {
		sendMessage("n -c " + capsuleName,
				MDebuggerTCPSocket.Command.STEPPING);
		// Ugly timer to be sure to leave amount of time for the debugger to evaluate the next event
		// Could be corrected if the command does not only return ACK
		try {
			System.out.println("sleeping 2000ms");
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sendMessage("v -c "+ capsuleName +" -n 5 -e",
				MDebuggerTCPSocket.Command.WAITING_FOR_LAST_EVENT);
	}
	
	public void requestLastEvent() {
	}
	
	private void sendMessage(String message, Command cmd) {
		message = message + " -i " + ++commandId; 
		String messageSize = String.valueOf(message.length());
		int length = messageSize.length();
		
		for (int i = 4; i > length; i--) {
			messageSize = "0" + messageSize;
		}
		
		message = messageSize + message;
		
		try {
			System.out.println("sending message: '" + message + "'");
			this.reader.getCommandStack().put(commandId, cmd);
			out.write(message.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
