/*******************************************************************************
 * Copyright (c) 2016 School of Computing -- Queen's University
 *
 * Description: no description currently
 * 
 * Contributors:
 *     Nicolas Hili <hili@cs.queensu.ca> - initial API and implementation
 ******************************************************************************/
package ca.queensu.cs.mdebugger.debugger.breakpoints;

import org.eclipse.debug.core.model.Breakpoint;

import ca.queensu.cs.mdebugger.debugger.Activator;

/**
 * @author nicolas
 *
 */
public class TransitionBreakpoint extends Breakpoint {

	/**
	 * 
	 */
	public TransitionBreakpoint() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.model.IBreakpoint#getModelIdentifier()
	 */
	@Override
	public String getModelIdentifier() {
		return Activator.PLUGIN_ID;
	}
	
	

}
