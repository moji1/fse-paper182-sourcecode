/*******************************************************************************
 * Copyright (c) 2016 School of Computing -- Queen's University
 *
 * Description: no description currently
 * 
 * Contributors:
 *     Nicolas Hili <hili@cs.queensu.ca> - initial API and implementation
 ******************************************************************************/
package ca.queensu.cs.mdebugger.debugger.model;

import org.eclipse.debug.core.DebugEvent;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.model.IBreakpoint;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IStackFrame;
import org.eclipse.debug.core.model.IThread;
import org.eclipse.debug.core.model.IVariable;

import ca.queensu.cs.mdebugger.debugger.stackframes.CapsuleStackFrame;
import ca.queensu.cs.mdebugger.debugger.variables.MDebuggerVariable;

/**
 * @author nicolas
 *
 */
public class CapsuleThread extends MDebuggerDebugElement implements IThread {

	private String capsuleName;
	
	private String[] events;
	
	private String currentState;
	

	/**
	 * @param target
	 */
	public CapsuleThread(IDebugTarget target, String capsuleName) {
		super(target);
		this.capsuleName = capsuleName;
		this.currentState = "";
	//	this.stackFrames = new CapsuleStackFrame[0];
	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.model.ISuspendResume#canResume()
	 */
	@Override
	public boolean canResume() {
		return this.getDebugTarget().canResume();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.model.ISuspendResume#canSuspend()
	 */
	@Override
	public boolean canSuspend() {
		return this.getDebugTarget().canSuspend();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.model.ISuspendResume#isSuspended()
	 */
	@Override
	public boolean isSuspended() {
		return this.getDebugTarget().isSuspended();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.model.ISuspendResume#resume()
	 */
	@Override
	public void resume() throws DebugException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.model.ISuspendResume#suspend()
	 */
	@Override
	public void suspend() throws DebugException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.model.IStep#canStepInto()
	 */
	@Override
	public boolean canStepInto() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.model.IStep#canStepOver()
	 */
	@Override
	public boolean canStepOver() {
		return this.isSuspended();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.model.IStep#canStepReturn()
	 */
	@Override
	public boolean canStepReturn() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.model.IStep#isStepping()
	 */
	@Override
	public boolean isStepping() {
		return isSuspended();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.model.IStep#stepInto()
	 */
	@Override
	public void stepInto() throws DebugException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.model.IStep#stepOver()
	 */
	@Override
	public void stepOver() throws DebugException {
		((MDebuggerDebugTarget)this.getDebugTarget()).stepOver(this.getCapsuleName());
	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.model.IStep#stepReturn()
	 */
	@Override
	public void stepReturn() throws DebugException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.model.ITerminate#canTerminate()
	 */
	@Override
	public boolean canTerminate() {
		return this.getDebugTarget().canTerminate();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.model.ITerminate#isTerminated()
	 */
	@Override
	public boolean isTerminated() {
		return this.getDebugTarget().isTerminated();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.model.ITerminate#terminate()
	 */
	@Override
	public void terminate() throws DebugException {
	/*	for (IStackFrame stackFrame: this.stackFrames)
			stackFrame.terminate();
		
		stackFrames = new CapsuleStackFrame[0];
		*/
		fireTerminateEvent();
		
	}
	

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.model.IThread#getTopStackFrame()
	 */
	@Override
	public IStackFrame getTopStackFrame() throws DebugException {
		System.out.println("first stack frame");
		if (!this.isSuspended() || this.events == null)
			return null;
		
		// retrieving the events
		CapsuleStackFrame stackFrame = new CapsuleStackFrame(this, ""); // we remove the two first arguments
		
		String event = events[0];
		
		// Retrieving each value
		String[] eventInfos = event.split(";");
		String eventSourceKind = eventInfos[1];
		String eventType = eventInfos[2];
		String capsuleName = eventInfos[3];
		String capsulePartName = eventInfos[4];
		int capsuleIndex = Integer.parseInt(eventInfos[5]);
		String currentState = eventInfos[6];
		int weirdNumber = Integer.parseInt(eventInfos[7]);
		int timestamp = Integer.parseInt(eventInfos[8]);
		int timestampNano = Integer.parseInt(eventInfos[9]);
		String[] var = eventInfos[10].split("\n");
		// TODO payload parameters
		
		//stackFrame.suspend();
		
		stackFrame.setTimestamp(timestamp);
		stackFrame.setTimestampNano(timestampNano);
		stackFrame.setCurrentState(currentState);
		stackFrame.setEventSourceKind(eventSourceKind);
		stackFrame.setEventType(eventType);
		stackFrame.setWeirdNumber(weirdNumber);
		stackFrame.setVariables(var);
		
		stackFrame.setName("["+timestamp+"]: " + stackFrame.getEventType());
			
		return stackFrame;
				
	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.model.IThread#getStackFrames()
	 */
	@Override
	public IStackFrame[] getStackFrames() throws DebugException {
		System.out.println(this.getName());
		
		if (!this.isSuspended() || this.events == null)
			return new IStackFrame[0];
		
		// retrieving the events
		IStackFrame[] stackFrames = new CapsuleStackFrame[events.length]; // we remove the two first arguments
		
		for (int i = 0; i < events.length; i++) {
			
			// Instantiating a stackframe per event
			CapsuleStackFrame stackFrame = new CapsuleStackFrame(this, "");

			String event = events[i];
			
			// Retrieving each value
			String[] eventInfos = event.split(";");
			String eventSourceKind = eventInfos[1];
			String eventType = eventInfos[2];
			String capsuleName = eventInfos[3];
			String capsulePartName = eventInfos[4];
			int capsuleIndex = Integer.parseInt(eventInfos[5]);
			String currentState = eventInfos[6];
			int weirdNumber = Integer.parseInt(eventInfos[7]);
			int timestamp = Integer.parseInt(eventInfos[8]);
			int timestampNano = Integer.parseInt(eventInfos[9]);
			String[] var = eventInfos[10].split("\n");
			// TODO payload parameters
			
			//stackFrame.suspend();
			
			stackFrame.setTimestamp(timestamp);
			stackFrame.setTimestampNano(timestampNano);
			stackFrame.setCurrentState(currentState);
			stackFrame.setEventSourceKind(eventSourceKind);
			stackFrame.setEventType(eventType);
			stackFrame.setWeirdNumber(weirdNumber);
			stackFrame.setVariables(var);
			stackFrame.setName("["+timestamp+"]: " + stackFrame.getEventType());

			if (i > 0)
				((CapsuleStackFrame) stackFrames[i-1]).compareToPreviousStackFrame(stackFrame);
			
			stackFrames[i] = stackFrame;
			
				
			
		}
		
		return stackFrames;
	}
	
	public void setEvents(String[] events) {
		this.events = events;
		
		String currentState = events[0].split(";")[6];
		this.setCurrentState(currentState);
		fireChangeEvent(DebugEvent.CHANGE);
	}
	
	public void pushEvent(String event) {
		
		int l = events.length;
		
		for (int i = l-2; i >= 0; i--) {
			events[i+1] = events[i];
		}
		
		String currentState = event.split(";")[6];
		this.setCurrentState(currentState);
		
		fireChangeEvent(DebugEvent.CHANGE);
		
		events[0] = event;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.model.IThread#hasStackFrames()
	 */
	@Override
	public boolean hasStackFrames() throws DebugException {
		return this.events != null  && this.isSuspended();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.model.IThread#getPriority()
	 */
	@Override
	public int getPriority() throws DebugException {
		return 0;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.model.IThread#getName()
	 */
	@Override
	public String getName() throws DebugException {
		String name = this.capsuleName;
		if (!this.currentState.equals(""))
			name += " ["+currentState+"]";
		return name;
	}
	
	public String getCapsuleName() {
		return this.capsuleName;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.model.IThread#getBreakpoints()
	 */
	@Override
	public IBreakpoint[] getBreakpoints() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void modifyValue(String name, String expression) {
		((MDebuggerDebugTarget)this.getDebugTarget()).modifyValue(this.getCapsuleName(), name, expression);
	}

	public String getCurrentState() {
		return currentState;
	}

	public void setCurrentState(String currentState) {
		String[] currentStateFragment = currentState.split("__");
		if (currentStateFragment.length >= 9)
			currentState = currentStateFragment[9];
		this.currentState = currentState;
	}

}
