/*
 * UMLRTDebugger.h
 *
 *  Created on: Nov 22, 2016
 *      Author: mojtababagherzadeh
 */

#ifndef UMLRTDEBUGGER_H_
#define UMLRTDEBUGGER_H_
#include "CapsuleTracker.h"
#include "SharedMem.h"
#include "Config.h"
#include "DebugCommand.h"
#include <netdb.h>
#include <netinet/in.h>
#include "TCPClient.h"
#include <thread>
#include <condition_variable>
#include <mutex>


#include "CmdInterface.h"
namespace mdebugger {
static const std::string  DEBUG_PREFIX="__Debug__";
struct TCPServer{
	int sockfd, newsockfd, portno, client;
	sockaddr_in serv_addr, cli_addr;
};

class UMLRTDebugger {
public:
	UMLRTDebugger();
	virtual ~UMLRTDebugger();
	void readEvents();
	void sendCommand(std::string cmdStr);
	void intialize();
	void handelCleint();
	void handelExtTCPSrvConn();//(std::string host, int port);
	int  getCaspuleIndex(std::string capsuleQualifiedName,debugEvents::Event event);
	void addCapsule(mdebugger::CapsuleTracker);
	void cmdMainLoop();
	void processUserCommnad();
	std::string processExtCommnad(mdebugger::CmdInterface cmd);
	void listCapsules();
	void listCapsuleConfig(std::string capsuleQualifiedName);
	void listCapsuleBreakPoints(std::string capsuleQualifiedName);
	std::string serializeCapsuleConfig(std::string capsuleQualifiedName);
	std::string serializeCapsuleBreakPoints(std::string capsuleQualifiedName);
	void viewCapsuleAttributes(std::string capsuleQualifiedName);
	void viewCapsuleEvents(std::string capsuleQualifiedName,int count);
	std::string serializeCapsuleAttributes(std::string capsuleQualifiedName);
	std::string serializeCapsuleEvents(std::string capsuleQualifiedName,int count);
	void stepExec(std::string capsuleQualifiedName);
	void setBreakPoint(std::string capsuleQualifiedName,std::string transName,char location);
	void setExecMode(std::string capsuleQualifiedName,mdebugger::EXECMODE execMode);
	void remBreakPoint(std::string capsuleQualifiedName,std::string transName,char location);
	const std::string  serializeCapsuleList();
	void modifyCapsuleAttribute(std::string capsuleQualifiedName,std::string varName,std::string varValue);
	Comms::TCPClient   clientConn;
private:
	std::vector<mdebugger::CapsuleTracker> capsules;
	std::map<std::string,int> capsuleMap;
	Comms::SharedMem eventQ;
	Comms::SharedMem commandQ;
	ConfigUtil::Config config;
	void intializeTCP();
	void intializeSHM();
	std::mutex eventMutex;
	std::mutex commandMutex;
	bool  commandIsReady;
	bool  eventIsReady;
	std::condition_variable eventCond;
	std::condition_variable cmdCond;
	std::thread extCon;
	CmdInterface cmdLineInterface;
};

} /* namespace mdebugger */

#endif /* UMLRTDEBUGGER_H_ */
