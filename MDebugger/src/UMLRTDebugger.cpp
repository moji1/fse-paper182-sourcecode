/*
 * UMLRTDebugger.cpp
 *
 *  Created on: Nov 22, 2016
 *      Author: mojtababagherzadeh
 */

#include "UMLRTDebugger.h"


namespace mdebugger {


UMLRTDebugger::UMLRTDebugger() {
	// TODO Auto-generated constructor stub
	eventIsReady=false;
	commandIsReady=false;

}

UMLRTDebugger::~UMLRTDebugger() {
	this->extCon.join();
	// TODO Auto-generated destructor stub
}

} /* namespace mdebugger */

void mdebugger::UMLRTDebugger::intialize() {
	this->intializeSHM();
}

void mdebugger::UMLRTDebugger::readEvents() {
	 bool newEvent=false;
	 while (true){
	    std::string tempStr=this->eventQ.safePopBackString(); // for a reason that i don't know push_back and pop back works like pushback and popfront, that is wired
	    //std::cout<<"try read event\n";
	    if (tempStr!=""){
	    	debugEvents::Event e1;
	    	e1.deserialize(tempStr);
	    	//std::cout<<"Event reader-> waiting for lock\n";
	    	std::unique_lock<std::mutex> l1(eventMutex);
	    	//std::cout<<"Event reader-> lock acquired \n";
	    	int cpasuleIndex=this->getCaspuleIndex(e1.getOwnerName(),e1);
	    	if (cpasuleIndex>=0){
	    		this->capsules[cpasuleIndex].addEvent(e1);
	    		if (e1.getEventSourceKind()==debugEvents::STATE && e1.getEventType()==debugEvents::STATEENTRYSTART &&
	    				e1.getSourceName()!=this->capsules[cpasuleIndex].getCurrentState()){
	    			this->capsules[cpasuleIndex].setCurrentState(e1.getSourceName());
	    			//std::cout<<e1.getSimpleSourceName()<<"\n";
	    			/// if capsule running mode is Run and the state is added by instruemnation send stepExec command
	    			if (this->capsules[cpasuleIndex].getExecMode()==mdebugger::EXECMODE::RUN &&
	    					e1.getSimpleSourceName().find(DEBUG_PREFIX)!=std::string::npos ){
	    				this->stepExec(e1.getOwnerName());
	    				//std::cout<<"Step exec beacuse of STATEENTRY and RUN mode"<<"\n";
	    			}
	    			else if (this->capsules[cpasuleIndex].getExecMode()==mdebugger::EXECMODE::CONTINUE &&
	    					e1.getSimpleSourceName().find(DEBUG_PREFIX)!=std::string::npos &&
							!(capsules[cpasuleIndex].existBreakPoints(e1.getSimpleSourceName())) ){
	    				this->stepExec(e1.getOwnerName());
	    			}
	    		}
	    		newEvent=true;
	    	}

	    }
	    /*else if (!eventIsReady and newEvent ){
	    	eventIsReady=true;
	    	newEvent=false;
	    }
	    else if (eventIsReady){
	    	eventCond.notify_all();
	    	eventIsReady=false;
	    }*/
	    else sleep(1);
	 }
}

int mdebugger::UMLRTDebugger::getCaspuleIndex(std::string capsuleOwner,debugEvents::Event event) {
	if (capsuleOwner.length()>=1)
	if (this->capsuleMap.count(capsuleOwner))
			return this->capsuleMap.at(capsuleOwner);
		else{
			int n=this->capsuleMap.size();
			this->capsuleMap[capsuleOwner]=n;
			mdebugger::CapsuleTracker c1;
			c1.setCapsuleName(event.getCapsuleName());
			c1.setCapsuleInstance(event.getCapsuleInstance());
			c1.setCapsuleIndex(event.getCapsuleIndex());
			this->capsules.push_back(c1);
			return capsuleMap.at(capsuleOwner);
		}
	else
		return -1;
}

void mdebugger::UMLRTDebugger::addCapsule(mdebugger::CapsuleTracker capsuleTracker) {
	this->capsules.push_back(capsuleTracker);
}

void mdebugger::UMLRTDebugger::listCapsuleConfig(std::string capsuleName) {
	for(std::map<std::string,int>::const_iterator it = this->capsuleMap.begin(); it != this->capsuleMap.end(); ++it)
		if (it->first.find(capsuleName) != std::string::npos){
			//std::cout<<it->second <<") "<<it->first<<"\n";
			//std::cout<<std::setw(30)<<std::setfill('-')<<"\n";
			std::cout<<"Current State is \""<<this->capsules[it->second].getCurrentState()<<"\"\n";
			std::cout<<"Running Mode is  \""<<this->capsules[it->second].getExecModeStr()<<"\"\n";
			std::cout<<"Breakpoints:\n"; //
			for (int i=0;i<this->capsules[it->second].getBreakPoints().size();i++)
				std::cout<<i+1<<") "<<this->capsules[it->second].getBreakPoints()[i]<<"\n";
			}

}

void mdebugger::UMLRTDebugger::listCapsuleBreakPoints(std::string capsuleName) {
	for(std::map<std::string,int>::const_iterator it = this->capsuleMap.begin(); it != this->capsuleMap.end(); ++it)
		if (it->first.find(capsuleName) != std::string::npos){
			//std::cout<<it->second <<"- "<<it->first<<"\n";
			std::cout<<"Breakpoints:\n"; //
			for (int i=0;i<this->capsules[it->second].getBreakPoints().size();i++)
				std::cout<<i+1<<") "<<this->capsules[it->second].getBreakPoints()[i]<<"\n";
			}
}

void mdebugger::UMLRTDebugger::viewCapsuleAttributes(std::string capsuleName) {
	//std::cout<<"show variables\n";
	for(std::map<std::string,int>::const_iterator it = this->capsuleMap.begin(); it != this->capsuleMap.end(); ++it)
		if (it->first.find(capsuleName) != std::string::npos){
			//std::cout<<it->second <<") "<<it->first<<"\n";
			//this->capsules[it->second].processLiveEvent();
			std::cout<<"Current State is \""<<this->capsules[it->second].getCurrentState()<<"\"\n";
			std::cout<<"----------- "<<capsuleName<<" Capsule's Variables---------------------------\n"; //
			std::cout<<this->capsules[it->second].getLastEvent().getVariableData()<<"\n";
			//std::cout<<this->capsules[it->second].getLastEvent().getEventId()<<"\n";
			//std::cout<<"-----------------------------------------------------------------------------\n";
			}

}

void mdebugger::UMLRTDebugger::viewCapsuleEvents(std::string capsuleName,int count) {
	for(std::map<std::string,int>::const_iterator it = this->capsuleMap.begin(); it != this->capsuleMap.end(); ++it)
		if (it->first.find(capsuleName) != std::string::npos){
			//std::cout<<it->second <<"- "<<it->first<<"\n";
			//this->capsules[it->second].processLiveEvent();
			std::cout<<"Current State is \""<<this->capsules[it->second].getCurrentState()<<"\"\n";
			//std::cout<<"-----------last  "<<count<<" "<<capsuleName<<" Capsule's Events---------------------------\n"; //
			std::vector<debugEvents::Event> events=this->capsules[it->second].lastNEvents(count);
			for (int i=0;i<events.size();i++)
				std::cout<<i+1<<"- "<<events[i];
			}

}

void mdebugger::UMLRTDebugger::sendCommand(std::string cmdStr) {
	commandQ.safePushBackString(cmdStr);

}

void mdebugger::UMLRTDebugger::stepExec(std::string qualifiedName) {
	mdebugger::DebugCommand dbgCmd;
	//std::cout<<qualifiedName;
	dbgCmd.setQualifiedName(qualifiedName);
	dbgCmd.setCmdId(mdebugger::dbgCmd::EXEC);
	dbgCmd.generateTraceNo();
	sendCommand(dbgCmd.serialize());
}

void mdebugger::UMLRTDebugger::setBreakPoint(std::string capsuleName,std::string transName, char location) {
	std::stringstream ss;
	ss<<DEBUG_PREFIX;
	ss<<location;
	ss<<"__S__";
	ss<<transName;
	for(std::map<std::string,int>::const_iterator it = this->capsuleMap.begin(); it != this->capsuleMap.end(); ++it)
		if (it->first.find(capsuleName) != std::string::npos)
			this->capsules[it->second].setBreakPoints(ss.str());


}

void mdebugger::UMLRTDebugger::setExecMode(std::string capsuleName,mdebugger::EXECMODE execMode) {
	for(std::map<std::string,int>::const_iterator it = this->capsuleMap.begin(); it != this->capsuleMap.end(); ++it)
		if (it->first.find(capsuleName) != std::string::npos)
			this->capsules[it->second].setExecMode(execMode);

}

void mdebugger::UMLRTDebugger::remBreakPoint(std::string capsuleName,std::string transName, char location) {
	std::stringstream ss;
	ss<<DEBUG_PREFIX;
	ss<<location;
	ss<<"__S__";
	ss<<transName;
	for(std::map<std::string,int>::const_iterator it = this->capsuleMap.begin(); it != this->capsuleMap.end(); ++it)
		if (it->first.find(capsuleName) != std::string::npos)
			this->capsules[it->second].remBreakPoint(ss.str());

}

const std::string  mdebugger::UMLRTDebugger::serializeCapsuleList(){
	std::stringstream ss;
	bool firstRec=true;
	for(std::map<std::string,int>::const_iterator it = this->capsuleMap.begin(); it != this->capsuleMap.end(); ++it){
		if (firstRec)
			firstRec=false;
		else
			ss<<"&";
		ss<<it->first<<"|";
		ss<<this->capsules[it->second].getCurrentState();
	}
	return ss.str();

}
void mdebugger::UMLRTDebugger::handelExtTCPSrvConn(){//(std::string host, int port) {
	//Comms::TCPClient  * clientConn= new Comms::TCPClient(port,host);
	//if (clientConn.checkConnectionStatus()!=0){
		//std::cout<<host<<":"<<port<<"\n";
		if (clientConn.conn()==0 && clientConn.checkConnectionStatus()==0){
			mdebugger::CmdInterface extCmd;
			std::string tempS="";
			{
				std::unique_lock<std::mutex> lock(this->eventMutex);
				tempS=this->serializeCapsuleList();
			}
			if (clientConn.sendDataWithLen("0000|"+tempS))
				while (true){ // the map loop for handling request
					char * buffer;
					if (clientConn.receiveWithLen(&buffer)>0){
						tempS="";
						tempS.append(buffer);
						free(buffer);
						extCmd.setCommandStr(tempS);
						extCmd.tokenizeCommand();
						extCmd.parseCommand();
						std::string cmdTraceId="";
						if (extCmd.getParsedCmd().commandOptions.count("-i")==1){ // echo the command id
							mdebugger::ParsedCMD tempParsedCmd=extCmd.getParsedCmd();
							cmdTraceId=tempParsedCmd.commandOptions["-i"];
						}
						clientConn.sendDataWithLen(cmdTraceId+"|"+processExtCommnad(extCmd));
					}
					else{
						clientConn.closeConn();
						break;
					}
				}
		}
	//}
}

std::string mdebugger::UMLRTDebugger::serializeCapsuleConfig(std::string capsuleQualifiedName) {
	std::stringstream ss;
	bool firstRec=true;
	for(std::map<std::string,int>::const_iterator it = this->capsuleMap.begin(); it != this->capsuleMap.end(); ++it)
			if (it->first.find(capsuleQualifiedName) != std::string::npos){
				if (! firstRec)
					ss<<"&";
				else
					firstRec=false;
				ss<<it->first<<"|";
				ss<<this->capsules[it->second].getCurrentState()<<"|";
				ss<<this->capsules[it->second].getExecModeStr();
				for (int i=0;i<this->capsules[it->second].getBreakPoints().size();i++){
					ss<<"|";
					ss<<this->capsules[it->second].getBreakPoints()[i];
				}
			}
	return ss.str();
}

std::string mdebugger::UMLRTDebugger::serializeCapsuleBreakPoints(std::string capsuleQualifiedName) {
	std::stringstream ss;
	bool firstRec=true;
	for(std::map<std::string,int>::const_iterator it = this->capsuleMap.begin(); it != this->capsuleMap.end(); ++it)
		if (it->first.find(capsuleQualifiedName) != std::string::npos){
			if (! firstRec)
				ss<<"&";
			else
				firstRec=false;
			ss<<it->first;
			for (int i=0;i<this->capsules[it->second].getBreakPoints().size();i++){
				ss<<"|";
				ss<<this->capsules[it->second].getBreakPoints()[i];
			}
		}
	return ss.str();
}

std::string mdebugger::UMLRTDebugger::serializeCapsuleAttributes(std::string capsuleQualifiedName) {
	std::stringstream ss;
	bool firstRec=true;
	for(std::map<std::string,int>::const_iterator it = this->capsuleMap.begin(); it != this->capsuleMap.end(); ++it)
		if (it->first.find(capsuleQualifiedName) != std::string::npos){
			if (! firstRec)
				ss<<"&";
			else
				firstRec=false;
			ss<<it->first<<"|";
			ss<<this->capsules[it->second].getCurrentState()<<"|";
			ss<<this->capsules[it->second].getLastEvent().getVariableData();
			}
	return ss.str();

}

std::string mdebugger::UMLRTDebugger::serializeCapsuleEvents(std::string capsuleQualifiedName, int count) {
	std::stringstream ss;
	bool firstRec=true;
	for(std::map<std::string,int>::const_iterator it = this->capsuleMap.begin(); it != this->capsuleMap.end(); ++it)
		if (it->first.find(capsuleQualifiedName) != std::string::npos){
			if (! firstRec)
				ss<<"&";
			else
				firstRec=false;
			ss<<it->first<<"|";
			ss<<this->capsules[it->second].getCurrentState();
			std::vector<debugEvents::Event> events=this->capsules[it->second].lastNEvents(count);
			for (int i=0;i<events.size();i++){
					ss<<"|";
					ss<<i+1<<events[i].serialize();
			}
		}
	return ss.str();
}

void mdebugger::UMLRTDebugger::modifyCapsuleAttribute(std::string capsuleQualifiedName, std::string varName,std::string varValue) {
	mdebugger::DebugCommand dbgCmd;
	dbgCmd.setQualifiedName(capsuleQualifiedName);
	dbgCmd.setCmdId(mdebugger::dbgCmd::VARCHANGE);
	dbgCmd.generateTraceNo();
	dbgCmd.setCmdParams(varName, varValue);
	sendCommand(dbgCmd.serialize());
}

void mdebugger::UMLRTDebugger::intializeSHM() {
	this->eventQ.setName("EventArea");
	this->eventQ.setQueueName("EventQ");
	this->eventQ.setSize(2048);
	this->eventQ.setWithLock(true);
	eventQ.setUp(client);
	this->commandQ.setName("CommandArea");
	this->commandQ.setQueueName("CommandQ");
	this->commandQ.setSize(2048);
	this->commandQ.setWithLock(true);
	commandQ.setUp(client);
}

void mdebugger::UMLRTDebugger::intializeTCP() {
}

void mdebugger::UMLRTDebugger::listCapsules(){
	std::cout<<"Running Capsule List:\n";
	for(std::map<std::string,int>::const_iterator it = this->capsuleMap.begin(); it != this->capsuleMap.end(); ++it){
			std::cout<<it->second <<"- "<<it->first<<"\n";
			//this->capsules[it->second].processLiveEvent();
			//std::cout<<std::setw(30)<<std::setfill('-')<<"\n";
			std::cout<<"Current State is \""<<this->capsules[it->second].getCurrentState()<<"\"\n";
			//std::cout<<std::setw(40)<<std::setfill('-')<<"\n";
		}
}

void mdebugger::UMLRTDebugger::cmdMainLoop() {
	while (true){
		cmdLineInterface.getCommand();
		cmdLineInterface.tokenizeCommand();
		if (cmdLineInterface.parseCommand())
			processUserCommnad();
		else
			std::cout<<"Command is invalid, please use help to see the commands and their options\n";
	}
}

void mdebugger::UMLRTDebugger::processUserCommnad() {
	mdebugger::ParsedCMD cmd=cmdLineInterface.getParsedCmd();
	switch(cmd.cmdID){
		case mdebugger::mdebuggerCommand::HELP:
			cmdLineInterface.showHelp();
			break;
		case mdebugger::mdebuggerCommand::LIST:
			if (cmd.commandOptions.count("-c")==1 && cmd.commandOptions.count("-b")==1){
				std::unique_lock<std::mutex> lock(this->eventMutex);
				listCapsuleBreakPoints(cmd.commandOptions["-c"]);
			}
			else if (cmd.commandOptions.count("-c")==1){
				std::unique_lock<std::mutex> lock(this->eventMutex);
				listCapsuleConfig(cmd.commandOptions["-c"]);
			}
			else {
				std::unique_lock<std::mutex> lock(this->eventMutex);
				listCapsules();}
			break;
		case mdebugger::mdebuggerCommand::VIEW:
			if (cmd.commandOptions.count("-c")==1 && cmd.commandOptions.count("-v")==1){
				std::unique_lock<std::mutex> lock(this->eventMutex);
				viewCapsuleAttributes(cmd.commandOptions["-c"]);
			}
			else if (cmd.commandOptions.count("-c")==1 && cmd.commandOptions.count("-e")==1){
				std::unique_lock<std::mutex> lock(this->eventMutex);
				if (cmd.commandOptions.count("-n")==1){
					if  (atoi(cmd.commandOptions["-n"].c_str())>0)
						viewCapsuleEvents(cmd.commandOptions["-c"], atoi(cmd.commandOptions["-n"].c_str()));
				}
				else
					viewCapsuleEvents(cmd.commandOptions["-c"], 5);
			}
			break;
		case mdebugger::mdebuggerCommand::NEXT:
			if (cmd.commandOptions.count("-c")==1 ){
				std::unique_lock<std::mutex> lock(this->eventMutex);
				std::cout<<"Step execution to next\n";
				setExecMode(cmd.commandOptions["-c"],mdebugger::EXECMODE::STEPOVER);
				stepExec(cmd.commandOptions["-c"]);
			}
			break;
		case mdebugger::mdebuggerCommand::BREAKPOINT:
			if (cmd.commandOptions.count("-c")==1 && cmd.commandOptions.count("-t")==1 && cmd.commandOptions.count("-b")==1 && cmd.commandOptions.count("-r")==1){
				std::unique_lock<std::mutex> lock(this->eventMutex);
				std::cout<<"Setting breakpoint at start of \n";
				remBreakPoint(cmd.commandOptions["-c"],cmd.commandOptions["-t"],'B');
			}
			else if (cmd.commandOptions.count("-c")==1 && cmd.commandOptions.count("-t")==1 && cmd.commandOptions.count("-e")==1 && cmd.commandOptions.count("-r")==1){
				std::unique_lock<std::mutex> lock(this->eventMutex);
				std::cout<<"Setting breakpoint at start of \n";
				remBreakPoint(cmd.commandOptions["-c"],cmd.commandOptions["-t"],'E');
			}
			else if (cmd.commandOptions.count("-c")==1 && cmd.commandOptions.count("-t")==1 && cmd.commandOptions.count("-b")==1){
				std::unique_lock<std::mutex> lock(this->eventMutex);
				std::cout<<"Setting breakpoint at start of \n";
				setBreakPoint(cmd.commandOptions["-c"],cmd.commandOptions["-t"],'B');
			}
			else if (cmd.commandOptions.count("-c")==1 && cmd.commandOptions.count("-t")==1 && cmd.commandOptions.count("-e")==1){
				std::unique_lock<std::mutex> lock(this->eventMutex);
				std::cout<<"Setting breakpoint at start of \n";
				setBreakPoint(cmd.commandOptions["-c"],cmd.commandOptions["-t"],'E');
			}
			break;
		case mdebugger::mdebuggerCommand::CONTINUEEXEC:
			if (cmd.commandOptions.count("-c")==1){
				std::unique_lock<std::mutex> lock(this->eventMutex);
				setExecMode(cmd.commandOptions["-c"],mdebugger::EXECMODE::CONTINUE);
				stepExec(cmd.commandOptions["-c"]);
			}
			break;
		case mdebugger::mdebuggerCommand::RUNEXEC:
			if (cmd.commandOptions.count("-c")==1){
				std::unique_lock<std::mutex> lock(this->eventMutex);
				setExecMode(cmd.commandOptions["-c"],mdebugger::EXECMODE::RUN);
				stepExec(cmd.commandOptions["-c"]);
			}
			break;
		case mdebugger::mdebuggerCommand::CONNECT:
			if (cmd.commandOptions.count("-h")==1 && cmd.commandOptions.count("-p")){
				std::cout<<"Starting TCP connection\n";
				std::string host=cmd.commandOptions["-h"];
				int port=atoi(cmd.commandOptions["-p"].c_str());
				clientConn.setPort(port);
				clientConn.setServerAddress(host);
				//std::thread extCon(&mdebugger::UMLRTDebugger::handelExtTCPSrvConn,this,std::cref(host),port);
				//this->extCon=std::thread(&mdebugger::UMLRTDebugger::handelExtTCPSrvConn,this,std::cref(host),port);
				this->extCon=std::thread(&mdebugger::UMLRTDebugger::handelExtTCPSrvConn,this);
				//extCon.detach();
			}
			break;
		case mdebugger::mdebuggerCommand::MODIFY:
			if (cmd.commandOptions.count("-c")==1 && cmd.commandOptions.count("-n") && cmd.commandOptions.count("-v")){
				modifyCapsuleAttribute(cmd.commandOptions["-c"],cmd.commandOptions["-n"],cmd.commandOptions["-v"]);
			}
			break;
		default:
			std::cout<<"command is unknown, please use help to see commands and their options\n";
			break;
			//cmdLineInterface.showPrompt();
	}
}

/////
std::string mdebugger::UMLRTDebugger::processExtCommnad(mdebugger::CmdInterface extCommand) {
	mdebugger::ParsedCMD cmd=extCommand.getParsedCmd();
	std::string cmdTraceId="";

	switch(cmd.cmdID){
		case mdebugger::mdebuggerCommand::LIST:
			if (cmd.commandOptions.count("-c")==1 && cmd.commandOptions.count("-b")==1){
				std::unique_lock<std::mutex> lock(this->eventMutex);
				return serializeCapsuleBreakPoints(cmd.commandOptions["-c"]);
			}
			else if (cmd.commandOptions.count("-c")==1){
				std::unique_lock<std::mutex> lock(this->eventMutex);
				return serializeCapsuleConfig(cmd.commandOptions["-c"]);
			}
			else {
				std::unique_lock<std::mutex> lock(this->eventMutex);
				return serializeCapsuleList();}
			return "INVALIDOPTION";
		case mdebugger::mdebuggerCommand::VIEW:
			if (cmd.commandOptions.count("-c")==1 && cmd.commandOptions.count("-v")==1){
				std::unique_lock<std::mutex> lock(this->eventMutex);
				return serializeCapsuleAttributes(cmd.commandOptions["-c"]);
			}
			else if (cmd.commandOptions.count("-c")==1 && cmd.commandOptions.count("-e")==1){
				std::unique_lock<std::mutex> lock(this->eventMutex);
				if (cmd.commandOptions.count("-n")==1){
					if  (atoi(cmd.commandOptions["-n"].c_str())>0)
						return serializeCapsuleEvents(cmd.commandOptions["-c"], atoi(cmd.commandOptions["-n"].c_str()));
				}
				else
					return serializeCapsuleEvents(cmd.commandOptions["-c"], 5);
			}
			return "INVALIDOPTION";
		case mdebugger::mdebuggerCommand::NEXT:
			if (cmd.commandOptions.count("-c")==1 ){
				std::unique_lock<std::mutex> lock(this->eventMutex);
				std::cout<<"Step execution to next\n";
				setExecMode(cmd.commandOptions["-c"],mdebugger::EXECMODE::STEPOVER);
				stepExec(cmd.commandOptions["-c"]);
				return "ACK|"+cmd.commandOptions["-c"];
			}
			return "INVALIDOPTION";
		case mdebugger::mdebuggerCommand::BREAKPOINT:
			if (cmd.commandOptions.count("-c")==1 && cmd.commandOptions.count("-t")==1 && cmd.commandOptions.count("-b")==1 && cmd.commandOptions.count("-r")==1){
				std::unique_lock<std::mutex> lock(this->eventMutex);
				std::cout<<"Setting breakpoint at start of \n";
				remBreakPoint(cmd.commandOptions["-c"],cmd.commandOptions["-t"],'B');
				return "ACK|"+cmd.commandOptions["-c"];
			}
			else if (cmd.commandOptions.count("-c")==1 && cmd.commandOptions.count("-t")==1 && cmd.commandOptions.count("-e")==1 && cmd.commandOptions.count("-r")==1){
				std::unique_lock<std::mutex> lock(this->eventMutex);
				std::cout<<"Setting breakpoint at start of \n";
				remBreakPoint(cmd.commandOptions["-c"],cmd.commandOptions["-t"],'E');
				return "ACK|"+cmd.commandOptions["-c"];
			}
			else if (cmd.commandOptions.count("-c")==1 && cmd.commandOptions.count("-t")==1 && cmd.commandOptions.count("-b")==1){
				std::unique_lock<std::mutex> lock(this->eventMutex);
				std::cout<<"Setting breakpoint at start of \n";
				setBreakPoint(cmd.commandOptions["-c"],cmd.commandOptions["-t"],'B');
				return "ACK|"+cmd.commandOptions["-c"];
			}
			else if (cmd.commandOptions.count("-c")==1 && cmd.commandOptions.count("-t")==1 && cmd.commandOptions.count("-e")==1){
				std::unique_lock<std::mutex> lock(this->eventMutex);
				std::cout<<"Setting breakpoint at start of \n";
				setBreakPoint(cmd.commandOptions["-c"],cmd.commandOptions["-t"],'E');
				return "ACK|"+cmd.commandOptions["-c"];
			}
			return "INVALIDOPTION";
		case mdebugger::mdebuggerCommand::CONTINUEEXEC:
			if (cmd.commandOptions.count("-c")==1){
				std::unique_lock<std::mutex> lock(this->eventMutex);
				setExecMode(cmd.commandOptions["-c"],mdebugger::EXECMODE::CONTINUE);
				stepExec(cmd.commandOptions["-c"]);
				return "ACK|"+cmd.commandOptions["-c"];
			}
		case mdebugger::mdebuggerCommand::RUNEXEC:
			if (cmd.commandOptions.count("-c")==1){
				std::unique_lock<std::mutex> lock(this->eventMutex);
				setExecMode(cmd.commandOptions["-c"],mdebugger::EXECMODE::RUN);
				stepExec(cmd.commandOptions["-c"]);
				return "ACK|"+cmd.commandOptions["-c"];
			}
			return "INVALIDOPTION";
		case mdebugger::mdebuggerCommand::MODIFY:
			if (cmd.commandOptions.count("-c")==1 && cmd.commandOptions.count("-n") && cmd.commandOptions.count("-v")){
				modifyCapsuleAttribute(cmd.commandOptions["-c"],cmd.commandOptions["-n"],cmd.commandOptions["-v"]);
				return "ACK|"+cmd.commandOptions["-c"];
			}
			return "INVALIDOPTION";
		default:
			return "INVALIDCOMMAND";
	}
}
