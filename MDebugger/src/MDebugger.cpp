//============================================================================
// Name        : MDebugger.cpp
// Author      : Mojtaba 
// Version     :
// Copyright   : 
// Description : MDebugger Main loop
//============================================================================

#include <iostream>
#include <thread>
#include "UMLRTDebugger.h"
//#include <typeinfo>




int main() {
	mdebugger::UMLRTDebugger debugger;
	std::cout << "MDebugger is started" << std::endl;
	debugger.intialize();
	std::thread eventRead(&mdebugger::UMLRTDebugger::readEvents,&debugger);
	std::thread cmdInterface(&mdebugger::UMLRTDebugger::cmdMainLoop,&debugger);
	eventRead.join();
	cmdInterface.join();
	return 0;
}
